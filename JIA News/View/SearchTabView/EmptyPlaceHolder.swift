//
//  EmptyPlaceHolder.swift
//  JIA News
//
//  Created by Jundullah Ilhaq Aulia on 12/10/22.
//

import SwiftUI

struct EmptyPlaceholderView: View {
    
    let text: String
    let image: Image?
    
    var body: some View {
        VStack(spacing: 8) {
            Spacer()
            if let image = self.image {
                image
                    .imageScale(.large)
                    .font(.system(size: 52))
            }
            Text(text)
            Spacer()
        }
    }
}

//struct EmptyPlaceHolder_Previews: PreviewProvider {
//    static var previews: some View {
//        EmptyPlaceHolder()
//    }
//}
