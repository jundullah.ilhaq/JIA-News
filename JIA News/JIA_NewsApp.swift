//
//  JIA_NewsApp.swift
//  JIA News
//
//  Created by Jundullah Ilhaq Aulia on 11/10/22.
//

import SwiftUI

@main
struct JIA_NewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
